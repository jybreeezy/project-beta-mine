import { useEffect, useState } from "react";


function VehicleForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();


        const url = 'http://localhost:8100/api/models/';

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type' : 'application/json',

                },

            });

            const data = await response.json();


            if (response.ok) {
                setFormData({
                    name: '',
                    picture_url: '',
                    manfacturer_id: '',
                });
                console.log('Vehicle created successfully');
            } else {
                console.error('Failed to create vehicle');
            }
        } catch (error) {
            console.error('Error creating vehicle', error);
        }
    };

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if(manufacturers.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a vehicle model</h1>
                        <form onSubmit={handleSubmit} id="create-vehicle-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.name}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.picture_url}
                                    placeholder="picture_url"
                                    required
                                    type="url"
                                    name="picture_url"
                                    id="picture_url"
                                    className="form-control"
                                />
                                <label htmlFor="name">Image URL</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select
                                    onChange={handleFormChange}
                                    value={formData.manufacturer_id}
                                    name="manufacturer_id"
                                    id="manufacturer_id"
                                    className={dropdownClasses} required>

                                    <option value="">Select a manufacturer</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                        )
                                        })}
                                </select>


                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default VehicleForm;
