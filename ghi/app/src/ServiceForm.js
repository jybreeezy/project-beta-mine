import { useEffect, useState } from "react";

function ServiceForm() {
    const [technicians, setTechnicians] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };


    useEffect(() => {
        fetchData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/appointments/';

        const {date, time, ...rest} = formData;
        const updatedFormData = {
            ...rest,
            date_time: `${formData.date}T${formData.time}`,
        };

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(updatedFormData),
                headers: {
                    'Content-Type' : 'application/json',

                },

            });

            const data = await response.json();


            if (response.ok) {
                setFormData({
                    vin: '',
                    customer: '',
                    date: '',
                    time: '',
                    technician: '',
                    reason: '',
                });
                console.log('Service appointment created successfully');
            } else {
                console.error('Failed to create service appointment');
            }
        } catch (error) {
            console.error('Error creating service appointment', error);
        }
    };

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if(technicians.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a service appointment</h1>
                        <form onSubmit={handleSubmit} id="create-service-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.vin}
                                    placeholder="vin"
                                    required
                                    type="text"
                                    name="vin"
                                    id="vin"
                                    className="form-control"
                                />
                                <label htmlFor="vin">Automobile Vin</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.customer}
                                    placeholder="customer"
                                    required
                                    type="text"
                                    name="customer"
                                    id="customer"
                                    className="form-control"
                                />
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.date}
                                    placeholder="date"
                                    required
                                    type="date"
                                    name="date"
                                    id="date"
                                    className="form-control"
                                />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.time}
                                    placeholder="time"
                                    required
                                    type="time"
                                    name="time"
                                    id="time"
                                    className="form-control"
                                />
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select
                                    onChange={handleFormChange}
                                    value={formData.technician}
                                    name="technician"
                                    id="technician"
                                    className={dropdownClasses} required>

                                    <option value="">Select a technician</option>
                                    {technicians.map(technician => {
                                        return (
                                        <option key={technician.employee_id} value={technician.employee_id}>
                                            {technician.employee_id}
                                        </option>
                                        )
                                        })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.reason}
                                    placeholder="reason"
                                    required
                                    type="text"
                                    name="reason"
                                    id="reason"
                                    className="form-control"
                                />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ServiceForm;
