import React, {useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {

      const fetchManufacturers = async () => {
        try {
          const response = await fetch('http://localhost:8100/api/manufacturers/')
          const data = await response.json();
          setManufacturers(data.manufacturers);
        } catch (error) {
          console.error('Error fetching manufacturers:', error);
        }
      };

      fetchManufacturers();
    }, []);

    return (
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                        <tr key={manufacturer.href}>
                            <td>{ manufacturer.name }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}
export default ManufacturerList;
