import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/manufacturers">
                Manufacturers
              </NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/manufacturers/new">
                Create a manufacturer
              </NavLink>
            </li>
          <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/models">
                  Vehicle Models
                </NavLink>
              </li>
          <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/models/new">
                  Create a Vehicle Model
                </NavLink>
              </li>
          <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/automobiles">
                  Automobiles
                </NavLink>
              </li>
          <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/automobiles/new">
                  Create an Automobile
                </NavLink>
            </li>
          <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/salespeople">
                Salespeople
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/salespeople/new">
                Create a Salesperson
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/customers">
                Customers
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/customers/new">
                Create a Customer
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/sales">
                Sales
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/sales/new">
                Record a new sale
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/saleshistory">
                Salesperson History
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/manufacturers">
                Manufacturers
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/manufacturers/new">
                Create a manufacturer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/models">
                Vehicle Models
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/models/new">
                Create a Vehicle Model
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/automobiles">
                Automobiles
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/automobiles/new">
                Create an Automobile
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/technicians">
                Technicians
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/technicians/new">
                Add a technician
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/service/">
                Service Appointment List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/service/new">
                Create a service appointment
              </NavLink>
            </li>            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/servicehistory">
                Service History
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
