import { useEffect, useState } from "react";


function SalesForm() {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [formData, setFormData] = useState({
        price: '',
        salesperson: '',
        customer: '',
        automobile: ''
    });

    const fetchDropdownData = async () => {
        const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
        }

        const customersResponse = await fetch('http://localhost:8090/api/customers/');
        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customers);
        }

        const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/');
        if (automobilesResponse.ok) {
            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.autos);

        }
    };
    useEffect(() => {
        fetchDropdownData();
    }, []);


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();


        const url = 'http://localhost:8090/api/sales/';

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type' : 'application/json',

                },

            });

            const data = await response.json();


            if (response.ok) {
                setFormData({
                    price: '',
                    salesperson: '',
                    customer: '',
                    automobile: '',
                });
                console.log('sale created successfully');
            } else {
                console.error('Failed to create sale');
            }
        } catch (error) {
            console.error('Error creating sale', error);
        }
    };




    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if(salespeople.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a sale</h1>
                        <form onSubmit={handleSubmit} id="create-sale-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.price}
                                    placeholder="price"
                                    required
                                    type="text"
                                    name="price"
                                    id="price"
                                    className="form-control"
                                />
                                <label htmlFor="price">Price</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <select
                                    onChange={handleFormChange}
                                    value={formData.salesperson}
                                    required
                                    name="salesperson"
                                    id="salesperson"
                                    className={dropdownClasses}
                                >
                                    <option value="">Select Sales Person</option>
                                    {salespeople.map(salesperson => (
                                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                            {`${salesperson.first_name} ${salesperson.last_name} (${salesperson.employee_id})`}
                                        </option>
                                    ))}
                                </select>
                                <label htmlFor="customer">Select Sales Person</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <select
                                    onChange={handleFormChange}
                                    value={formData.customer}
                                    required
                                    name="customer"
                                    id="customer"
                                    className={dropdownClasses}
                                >
                                    <option value="">Select a Customer</option>
                                    {customers.map(customer => (
                                        <option key={customer.id} value={customer.id}>
                                             {`${customer.name} (${customer.id})`}
                                        </option>
                                    ))}
                                </select>
                                <label htmlFor="vin">Select Customer</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <select
                                    onChange={handleFormChange}
                                    value={formData.automobile}
                                    required
                                    name="automobile"
                                    id="automobile"
                                    className={dropdownClasses}
                                >
                                    <option value="">Select VIN</option>
                                    {automobiles.map(automobile => (
                                        <option key={automobile.vin} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    ))}
                                </select>
                                <label htmlFor="automobile">Select VIN</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SalesForm;
