import { useEffect, useState } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [filterAppointments, setFilterAppointments] = useState([]);
    const [vin, setVin] = useState('');
    const [automobiles, setAutomobiles]= useState([]);

    const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
            setFilterAppointments(data.appointments);
        }
    };

    useEffect(() => {

        getAppointments();
        const fetchAutomobiles = async () => {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            const data = await response.json();
            setAutomobiles(data.autos);
        };
        fetchAutomobiles();
    }, []);

    const handleSearch = (e) => {
        e.preventDefault();
        setFilterAppointments(appointments.filter(app => app.vin === vin));
    };

    const isVip = (vin) => {
        const vins = automobiles.map((auto) => auto.vin)
        return vins.includes(vin)
    };

    return (
        <>
        <div>
            <h2>Service History</h2>
            <form onSubmit={handleSearch}>
                <input
                    type="text"
                    placeholder="Search by vin"
                    value={vin}
                    onChange={(e) => setVin(e.target.value)}/>
                    <button type="submit">
                        Search by VIN
                    </button>
            </form>
            <table className="table table-striped-columns">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filterAppointments.map((appointment) => {
                        return (
                            <tr key={appointment.id}>
                                <td>{ appointment.vin }</td>
                                <td>{ isVip(appointment.vin) ? 'Yes' : 'No' }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                                <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                                <td>{ appointment.technician.employee_id }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.status }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    );
}
export default ServiceHistory;
