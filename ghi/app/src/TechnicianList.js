import React, { useState, useEffect } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/technicians/');
        const data = await response.json();


        setTechnicians(data.technicians);
      } catch (error) {
        console.error('Error fetching technicians:', error);
      }
    };

    fetchTechnicians();
    }, []);

    return (
        <div>
            <h2>Technicians</h2>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
             <tbody>
                {technicians.map(technician => {
                    return (
                        <tr key={technician.employee_id}>
                            <td>{ technician.employee_id }</td>
                            <td>{ technician.first_name }</td>
                            <td>{ technician.last_name }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}
export default TechnicianList;
