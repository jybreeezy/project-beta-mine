import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleList from './VehicleList';
import VehicleForm from './VehicleForm';
import AutomobileList from './AutomobileList'
import AutomobileForm from './AutomobileForm';
import SalesPersonForm from './SalesPersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalesHistory from './SalesHistory';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import ServiceForm from './ServiceForm';
import ServiceList from './ServiceList';
import ServiceHistory from './ServiceHistory';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" index element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
            <Route index element={<ManufacturerList />} />
          </Route>
          <Route path="models">
            <Route path="new" element={<VehicleForm/>} />
            <Route index element={<VehicleList/>} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm/>} />
            <Route index element={<AutomobileList/>} />
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalesPersonForm/>} />
            <Route index element={<SalespeopleList/>} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm/>} />
            <Route index element={<CustomerList/>} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SalesForm/>} />
            <Route index element={<SalesList/>} />
          </Route>
          <Route path="saleshistory" element={<SalesHistory/>} />
          <Route path="manufacturers">
          <Route path="new" element={<ManufacturerForm />} />
              <Route index element={<ManufacturerList />} />
            </Route>
          <Route path="models">
            <Route path="new" element={<VehicleForm />} />
            <Route index element={<VehicleList />} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
            <Route index element={<AutomobileList />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
            <Route index element={<TechnicianList />} />
          </Route>
          <Route path="service">
            <Route path="new" element={<ServiceForm />} />
            <Route index element={<ServiceList />} />
          </Route>
          <Route path="servicehistory" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
