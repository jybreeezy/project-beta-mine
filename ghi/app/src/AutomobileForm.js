import { useEffect, useState } from "react";


function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();


        const url = 'http://localhost:8100/api/automobiles/';

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type' : 'application/json',

                },

            });

            const data = await response.json();


            if (response.ok) {
                setFormData({
                    color: '',
                    year: '',
                    vin: '',
                    model_id: ''
                });
                console.log('automobile created successfully');
            } else {
                console.error('Failed to create automobile');
            }
        } catch (error) {
            console.error('Error creating automobile', error);
        }
    };

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if(models.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create an automobile</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.color}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="color"
                                    id="color"
                                    className="form-control"
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.year}
                                    placeholder="year"
                                    required
                                    type="text"
                                    name="year"
                                    id="year"
                                    className="form-control"
                                />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.vin}
                                    placeholder="vin"
                                    required
                                    type="text"
                                    name="vin"
                                    id="vin"
                                    className="form-control"
                                />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select
                                    onChange={handleFormChange}
                                    value={formData.model_id}
                                    name="model_id"
                                    id="model_id"
                                    className={dropdownClasses} required>

                                    <option value="">Select a Vehicle Model</option>
                                    {models.map(model => {
                                        return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                        )
                                        })}
                                </select>


                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AutomobileForm;
