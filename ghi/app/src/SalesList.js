import React, { useState, useEffect } from 'react';

function SalesList() {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
        // console.log(data)

        setSales(data.sales);
      } catch (error) {
        console.error('Error fetching sales:', error);
      }
    };

    fetchSales();
  }, []);

   

  return (
    <div>

      <table className="table table-striped-columns">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Sales Person</th>
            <th>Customer</th>
            <th>Price</th>


          </tr>
        </thead>
        <tbody>
          {sales.map(sale => (
        <tr key={sale.automobile.vin}>
        <td>{sale.automobile.vin}</td>
        <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name} (${sale.salesperson.employee_id})`}</td>
        <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
        <td>{sale.price}</td>
      </tr>
          ))}
        </tbody>
      </table>

    </div>

    );
  }

  export default SalesList;
