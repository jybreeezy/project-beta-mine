import React, { useState, useEffect } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    const fetchSalespeople = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        const data = await response.json();

        setSalespeople(data.salespeople);
      } catch (error) {
        console.error('Error fetching salespeople:', error);
      }
    };

    fetchSalespeople();
  }, []);

  return (
    <div>
      <h2>Sales People</h2>

    <table className="table table-striped-columns">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {salespeople.map(salesperson => (
          <tr key={salesperson.employee_id}>
            <td>{salesperson.first_name}</td>
            <td>{salesperson.last_name}</td>
            <td>{salesperson.employee_id}</td>
          </tr>
        ))}
      </tbody>
    </table>
    </div>
  );
}

export default SalespeopleList;
