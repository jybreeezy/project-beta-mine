import React, { useState, useEffect } from 'react';

function VehicleList() {
    const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchModels = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();


        setModels(data.models);
      } catch (error) {
        console.error('Error fetching vehicles:', error);
      }
    };

    fetchModels();
  }, []);
  return (
    <div className="text-center">
        <h2>Vehicle Models</h2>
    <div className="d-flex flex-wrap justify-content-center">
        <div>
        {models.map(model => (
          <div key={model.href} className="card m-2 shadow">
            <img src={model.picture_url} className="card-img-top" alt={model.name} />
            <div className="card-body">
                <h3 className="card-title">{model.manufacturer.name} </h3>
                <h3 className="card-title">{model.name} </h3>
            </div>
            </div>

        ))}
    </div>
    </div>
    </div>
  );
}

export default VehicleList;
