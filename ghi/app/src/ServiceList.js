import React, { useState, useEffect } from 'react';

function ServiceList() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {

    const fetchAppointments = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        setAppointments(data.appointments);
      } catch (error) {
        console.error('Error fetching appointments:', error);
      }
    };

    fetchAppointments();

        const fetchAutomobiles = async () => {
          try {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            const data = await response.json();
            setAutomobiles(data.autos);
          } catch (error) {
            console.error('Error fetching appointments:', error);
          }
        };

        fetchAutomobiles();
    }, []);

    const handleCancelAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, { method: 'PUT' });
        if (response.ok) {
            setAppointments(appointments.filter((appointment) => appointment.id !== id));
        }
    };

    const handleFinishAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, { method: 'PUT' });
        if (response.ok) {
            setAppointments(appointments.filter((appointment) => appointment.id !== id));
        }
    };

    const isVip = (vin) => {
        const vins = automobiles.map((auto) => auto.vin)
        return vins.includes(vin)
    };

    return (
        <div>
            <h2>Service Appointments</h2>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>VIN Number</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
             <tbody>
                {appointments.filter((appointment) => appointment.status === 'created')
                .map((appointment) => {
                    return (
                        <tr key={appointment.vin}>
                            <td>{ appointment.vin }</td>
                            <td>{ isVip(appointment.vin) ? 'Yes' : 'No' }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                            <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                            <td>{ appointment.technician.employee_id }</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                { appointment.status !== 'cancel' ? (
                                    <>
                                    <button className="btn btn-danger"
                                    onClick={() => handleCancelAppointment(appointment.id)}>
                                        Canceled
                                    </button>
                                    <button className="btn btn-success"
                                    onClick={() => handleFinishAppointment(appointment.id)}>
                                        Finished
                                    </button>
                                    </>
                                ) : null}
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}
export default ServiceList;
