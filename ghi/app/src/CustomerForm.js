import { useEffect, useState } from "react";

function CustomerForm() {
    const [name, setName] = useState('');
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setName(data.name);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();


        const url = 'http://localhost:8090/api/customers/';

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type' : 'application/json',

                },

            });

            const data = await response.json();


            if (response.ok) {
                setFormData({
                    first_name: '',
                    last_name: '',
                    address: '',
                    phone_number: '',

                });
                console.log('Scustomer created successfully');
            } else {
                console.error('Failed to create customer');
            }
        } catch (error) {
            console.error('Error creating customer', error);
        }
    };

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a customer</h1>
                        <form onSubmit={handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.first_name}
                                    placeholder="first_name"
                                    required
                                    type="text"
                                    name="first_name"
                                    id="first_name"
                                    className="form-control"
                                />
                                <label htmlFor="name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.last_name}
                                    placeholder="last_name"
                                    required
                                    type="text"
                                    name="last_name"
                                    id="last_name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.address}
                                    placeholder="address"
                                    required
                                    type="text"
                                    name="address"
                                    id="address"
                                    className="form-control"
                                />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleFormChange}
                                    value={formData.phone_number}
                                    placeholder="phone_number"
                                    required
                                    type="text"
                                    name="phone_number"
                                    id="phone_number"
                                    className="form-control"
                                />
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default CustomerForm;
