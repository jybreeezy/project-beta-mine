from django.urls import path

from .views import (
    api_technicians,
    api_delete_technician,
    api_appointments,
    api_delete_appointment,
    api_canceled_appointment,
    api_finished_appointment,
)


urlpatterns = [
    path(
        "technicians/",
        api_technicians,
        name="api_technicians",
    ),
    path(
        "technicians/<int:pk>/",
        api_delete_technician,
        name="api_technician",
    ),
    path(
        "appointments/",
        api_appointments,
        name="api_appointments",
    ),
    path(
        "appointments/<int:pk>/",
        api_delete_appointment,
        name="api_appointments",
    ),
    path(
        "appointments/<int:id>/cancel/",
        api_canceled_appointment,
        name="api_cancelled",
    ),
    path(
        "appointments/<int:id>/finish/",
        api_finished_appointment,
        name="api_finished",
    ),
]
